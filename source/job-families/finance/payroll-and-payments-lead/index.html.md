---
layout: job_family_page
title: "Payroll and Payments Manager"
---

GitLab is adding the next essential member who can help drive operational improvements within our finance department. You will join our team in its early stages responsible for developing a highly efficient, world class accounting process. This person will administer and improve our global payroll and payments process. You will be responsible for 100% of payments to GitLab contributors globally.

## Manager

### Responsibilities

- Coordinate all global payroll processing operations.
- Ensure that all relevant data is processed and track through the relevant payroll systems.
- Responsible for working with local tax and legal advisors to ensure the company follows local regulations with respect to payroll.
- Work closely with GitLab contributors and the People Ops team to ensure timely response to questions and requests that have been processed.
- Complete filings for payroll subsidies, where applicable.
- Accounting lead for implementation of new payroll systems.
- Accounting lead for other global payroll systems.
- Establish and enforce proper accounting policies and principles.
- Manage Payroll Specialist - US, Payroll Specialist - EMEA, Expense Specialist
- Primary ownership for management and external reporting.
- Development and implementation of new procedures to enhance the workflow of payments to contributors.
- Primary contact point for external audit with respect to payroll matters.
- Coordinate with other auditors as needed.
- Support overall department goals and objectives.

### Requirements

- Proven work experience as a payroll administrator.
- Experience with large payroll platforms such as ADP or Paychex.
- Been responsible for at least one international payroll function.
- Public company experience with Sarbanes Oxley a plus.
- Experience with Netsuite a plus.
- Proficient with google sheets.
- Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision

## Lead

GitLab is adding the next essential member who can help drive operational improvements within our finance department.
You will join our team in its early stages responsible for developing a highly efficient, world class accounting process.
This person will administer and improve our global payroll and payments process. You will be responsible for 100% of
payments to GitLab contributors globally.

### Responsibilities

- Coordinate all global payroll processing operations. Currently the company runs payroll in five countries and has partners in another three countries that provide payroll solutions to contributors.
- Ensure that all relevant data is processed and track through the relevant payroll systems.
- Responsible for working with local tax and legal advisors to ensure the company follows local regulations with respect to payroll.
- Work closely with GitLab contributors and the People Ops team to ensure timely response to questions and requests that have been processed.
- Complete filings for payroll subsidies, where applicable.
- Accounting lead for implementation of new payroll system in the US.
- Accounting lead for other global payroll systems.
- Establish and enforce proper accounting policies and principles.
- Primary ownership for management and external reporting.
- Development and implementation of new procedures to enhance the workflow of payments to contributors.
- Primary contact point for external audit with respect to payroll matters.
- Coordinate with other auditors as needed.
- Support overall department goals and objectives.


### Requirements

- Proven work experience as a payroll administrator.
- Experience with large payroll platforms such as ADP or Paychex.
- Been responsible for at least one international payroll function.
- Public company experience with Sarbanes Oxley a plus.
- Experience with Netsuite a plus.
- Proficient with google sheets.
- Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision

## Senior Manager, Global Payroll and Payments

Sr Manager, Global Payroll and Payments is responsible for managing payroll and expense reimbursement for roughly 1000 team members in 60 countries. The main focus will be on providing strategic leadership, developing processes, and controls, along with managing the relationship with all payroll providers.


### Essential Duties and Responsibilities

- Manage overall global operations of payrolls and payments
- Leads the strategic direction for the payroll processes while meeting key controls and payroll compliance
- Manage payroll team
- Manage all employee payroll queries and communication, including the Payroll inbox and Slack channels
- Work with PeopleOps team to ensure our HRIS (BambooHR) provides the right information in the best way for payroll integration and reporting
- Develop internal relationships and vendor partnerships to improve process efficiencies
- Develop and prioritize processes to improve the payroll experience for team members
- Ensure all operational processes are controlled from a risk perspective, and have built-in control plans and assessments to regularly review their effectiveness
- Evaluate, develop and enforce the use of audit tools which support the payroll process
- Set up payroll processes in new countries as we expand operations in another country.
- Lead significant systems and process changes in the payroll function.
- Support month end close activities
- Responsible for Worker’s Compensation audits
- Review and analyze current payroll, benefits and tax procedures in order to recommend changes to Company policies resulting in best practices
- Oversees payroll audit process and reporting aspects of internal and external audits; identifies and resolves discrepancies.
- Ensure that all monthly, quarterly and annual payroll tax filings are met; ensure all local statutory payroll and fringe benefit reporting requirements are met.
- Ensure stock activity for US and international employees is properly processed and reported.
- Maintain compliance with all Federal, State, and local payroll tax laws
- Responsible for all system year-end tasks including the reset of 401K, FSA, HSA limits per yearly IRS guidelines
- Prepare and administrator W-2’s for US entities and annual tax documents for non-US entities
- Serve as a subject matter expert and point of contact for escalations and various ad hoc requests
- Manage the expense reimbursement process for all team members
- Special projects as assigned
- Mentor direct reports
- Be a role model for the team, and develop a strong sense of shared team vision
- Maintains professional and technical knowledge by attending educational workshops and reviewing professional publications
- Prepare and manage budget related to payroll programs


## Performance Indicators
- [Payroll accuracy for each check date](/handbook/finance/payroll/#payroll-accuracy-for-each-check-date--100)
- [Payroll journal entry reports submitted to Accounting](/handbook/finance/payroll/#payroll-journal-entry-reports-submitted-to-accounting--payroll-date--2-business-days)
- [Percentage of ineffective SOX Controls](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with a member of our Recruiting team.
* Next, candidates will be invited to schedule a first interview with our Controller.
* Next, candidates will be invited to schedule an interview with a People Business Partner.
* Candidates will then be invited to schedule an interview with our CFO.
* Finally, candidates may be asked to interview with our CEO.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
