---
layout: markdown_page
title: "Category Vision - Administration"
---

- TOC
{:toc}

| Category Attribute | Link | 
| ------ | ------ |
| [Stage](https://about.gitlab.com/handbook/product/categories/#hierarchy) | [Manage](https://about.gitlab.com/direction/manage) | 
| [Maturity](/direction/maturity/#maturity-plan) | [Not applicable](#maturity) |
| Labels | [admin dashboard](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=admin%20dashboard), [spam fighting](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=spam%20fighting) |

## Administration

After a GitLab instance has been set up, it must be maintained. The GitLab administrator manages configuration and user activity on an ongoing basis - our mission is to ensure that this power user has the tools and endpoints they need in order to manage their instance effectively and efficiently.

## Target audience and experience

GitLab administrators of complicated instances are who we're building for - these are privileged users with the keys to the castle, but likely with a multitude of applications to manage and limited amounts of time. Finding settings should be straightforward, and the admin area should provide an admin with the information and levers needed in order to effectively manage the instance and its users.

## Maturity

As the admin panel in GitLab is fairly application-specific, Administration is considered a non-marketing category without a [maturity level](/direction/maturity/) that can be compared to other competing solutions.

## How you can help

As with any category in GitLab, it's dependent on your ongoing feedback and contributions. Here's how you can help:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/651).
1. Find issues in this category accepting merge requests. [Here's an example query](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=admin%20dashboard&label_name[]=Accepting%20merge%20requests)!

## What's next & why

### Current

We're focusing on the needs of our internal customer, the GitLab Support team, by adding a number of improvements to make GitLab.com (and other large instances) easier to manage. Please see https://gitlab.com/groups/gitlab-org/-/epics/645.

## Top user issue(s)

[Adding search](https://gitlab.com/gitlab-org/gitlab-ce/issues/50145) to the admin area is a top need. As the admin panel only becomes more complex under the weight of more features and configuration, adding search will go a long way to keeping this area of GitLab navigable.

## Top internal customer issue(s)

The GitLab Support team has created [an epic](https://gitlab.com/groups/gitlab-org/-/epics/645). We need to also focus on the abuse team's needs in issues like [templating abuse reports](https://gitlab.com/gitlab-org/gitlab-ce/issues/47950).

## Top Vision Item(s)

MVC of an admin page for a group Owner.
