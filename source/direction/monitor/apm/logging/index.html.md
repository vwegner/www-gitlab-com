---
layout: markdown_page
title: "Category Vision - Logging"
---

- TOC
{:toc}
# Logging

| | |
| --- | --- |
| Stage | [Monitor](/direction/monitor/) |
| Maturity | [Minimal](/direction/maturity/) |

## Introduction and how you can help
Thanks for visiting this category strategy page on Logging in GitLab. This category belongs to and is maintained by the [APM](/handbook/engineering/development/ops/monitor/APM/) group of the Monitor stage.

This strategy is a work in progress, and everyone can contribute:
 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aapm&label_name[]=Category%3ALogging) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aapm&label_name[]=Category%3ALogging) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your Metrics usage, we'd especially love to hear your use case(s)  
 

## Background
A fundamental requirement for running applications is to have a centralized location to manage and review the logs. While manually reviewing logs could work with just a single node app server, once the deployment scales beyond one you need solution which can [aggregate and centralize them](https://gitlab.com/gitlab-org/gitlab-ee/issues/3711) for review.

Much like we do with Prometheus, GitLab can integrate with existing open source logging solutions to provide an integrated experience. 

## Target Audience and Experience
Being able to capture and review logs are an important tool for all users across the DevOps spectrum. From pure developers who may need to troubleshoot their application when it is running in a staging or review environment, as well as pure operators who are responsible for keeping production services online.

The target workflow includes a few important use cases:
1. Aggregating logs from multiple hosts/containers
1. Filtering by host, container, service, timespan, regex, and other criteria. These filtering options should align with the filter options and tags/labels of our other monitoring tools, like metrics.
1. Log alerts should also be able to be created, triggering alerts under specific user defined scenarios.

## What's Next & Why
The next step is to install Elasticsearch as a base for our aggregated logging. Once we complete our evaluation we will offer our users a centralize logging solution, within Gitlab UI, based on Elasticsearch, with the push of a button.

## Epics
* [Logging - viable](https://gitlab.com/groups/gitlab-org/-/epics/1348)
* [Logging - Complete](https://gitlab.com/groups/gitlab-org/-/epics/1966)

## Competitive Landscape
[Splunk](https://www.splunk.com) and [Elastic](https://www.elastic.co/) are the top two competitors in this space.

## Analyst Landscape
There does not seem to be a Forrester or Gartner analysis for this product category.

We will be reaching out and setting up meetings with analysts to get their insight soon.

## Top Customer Success/Sales Issue(s)
[Aggregated logs](https://gitlab.com/gitlab-org/gitlab-ee/issues/3711)

## Top Customer Issue(s)
[Aggregated logs](https://gitlab.com/gitlab-org/gitlab-ee/issues/3711)

## Top Internal Customer Issue(s)

## Top Direction Item(s)
- [Aggregated logs](https://gitlab.com/gitlab-org/gitlab-ee/issues/3711)
- [Application log filtering and search](https://gitlab.com/gitlab-org/gitlab-ce/issues/64919)
- [Application log alerts](https://gitlab.com/gitlab-org/gitlab-ee/issues/3626)

