---
layout: handbook-page-toc
title: "Paid time off at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Paid Time Off

Time away from work can be extremely helpful for maintaining a good work/life balance. GitLab encourages managers and leadership to set the example by taking time off when needed, and ensuring their reports do the same. This page is written with a focus on vacation. If taking time off for medical needs, whether physical or mental, you should follow the appropriate process in your location. If missing more than 25 days of work due to medical requirements, you should move onto Short Term Disability, or the equivalent in your location. However, the below recommendations on communicating your time off apply to time off for all reasons where possible and avoid concerns of health, safety, or job abandonment.

A support engineer remarked that “In the 3 months I've been at GitLab, I've taken more time off than the last 2 years at my previous job.”

Another great example of taking time off is a GitLab team-member taking a day to do some spring cleaning. Vacations don't have to be trips to exotic places, but instead could be taking some time for oneself at home.

It is so important to take time off to recharge batteries and refresh the mind so you can come back to GitLab with renewed energy and be prepared to do your best work ever!

If you are taking Parental Leave, please see our [Parental Leave Policy](/handbook/benefits/#parental-leave).

## A GitLab team-member's Guide to Time Off

As we all work remotely it can sometimes be difficult to know when and how to plan time off. Here is some advice and guidance on how this can be done in an easy and collaborative way.

1. We have a "no ask, must tell" time off policy. This means that:
    * You do not need to ask permission to take time off unless you want to have more than 25 consecutive calendar days off. The 25-day no ask limit is per vacation, not per year. You can have multiple no ask vacations per year that add up to more than 25 days in total; there is no limit to this. 
    * What we care about are your results, not how long you work. While you don't need to ask approval for time off, it shouldn’t be at the expense of business getting done. Please coordinate with your team before taking time off, especially during popular or official holidays, so that we can ensure business continuity.  We want to ensure we have adequate coverage and avoid situations where all/most of the team is taking time off at the same time. 
    * When taking time off make sure your manager is aware of your absence. Informing your manager can be done by using [PTO Ninja](#pto-ninja), as it will create an event and notify your manager for you.  Giving your manager and team members a heads up early helps them prioritize work and meet business goals and deadlines. 
    * If you're gone for 72 hours without notification, this could be deemed as [Job Abandonment](/handbook/people-group/code-of-conduct/#sts=Job%20Abandonment).
    * It can be helpful to take longer breaks to re-energize. If this is helpful to you, we strongly recommend taking at least two consecutive weeks of time off per year.
1. We don't frown on people taking time off, but rather encourage people to take care of themselves and others by having some time away. If you notice that your co-worker is working long hours over a sustained period, you may want to let them know about the time off policy.
1. Not taking vacation is viewed as a weakness and people shouldn't boast about it. It is viewed as a lack of humility about your role, not fostering collaboration in training others in your job, and not being able to document and explain your work. You are doing the company a disservice by being a single point of failure. The company must be able to go for long periods without you. We don't want to lose you permanently by you burning yourself out by not taking regular vacations.
1. Working hours are flexible, you are invited to the [company call](/handbook/communication/#company-call) if you are available, but it isn't mandatory and you shouldn't attend if it is during your time off. We encourage you to read the company call agenda on your return to catch up on the announcements made while you were on your time off. 
1. You don't need to worry about taking time off to go to the gym, [take a nap](https://m.signalvnoise.com/sleep-deprivation-is-not-a-badge-of-honor-f24fbff47a75), go grocery shopping, do household chores, help someone, take care of a loved one, etc. If you have urgent tasks, but something comes up or takes longer than expected, just ensure the rest of the team **knows** and someone can pick up the tasks (assuming you're able to communicate).
1. GitLab encourages team members to volunteer within their community to take care of others.
1. GitLab also encourages you to use your leave for jury duty, bereavement leave, or to vote. You are not expected to work during this time off, but we recommend following the guidance under [Communicating Your Time Off](#communicating-your-time-off) when these situations arise.
1. We encourage all team members to take their country of residence's official holidays off. Using [Time and Date](https://www.timeanddate.com/holidays/), you can see which holidays are considered official by selecting your country, clicking "Change Holidays", and choosing "Official Holidays".
    * We still **help** clients during official days off, unless they are official days off in both the Netherlands and the U.S. For any particular day, we try to always have people working from countries that aren't observing an official holiday. If you need to work during an official day off in your country, you should take a different day off in return.
1. Please also remember to turn on your out of office message and include the contact details of a co-worker in case anything urgent or critical comes into your inbox while you're away. If you have to respond to an incident while on-call outside of your regular working hours, you should feel free to take off some time the following day to recover and be well-rested. If you feel pressured to _not_ take time off to rest, refer to this part of the handbook and explain that you had to handle an incident.
1. When returning from paid time off, it can be helpful to schedule a [coffee chat](/company/culture/all-remote/informal-communication/#coffee-chats) or two on the day of your return to get caught up, share stories from your time off, and simply reconnect with your team members. It also provides a nice break from to-dos and unread emails. This type of conversation may occur organically in a colocated office but needs to be managed with intent in an all-remote company. 

### Communicating Your Time Off
Communicate broadly when you will be away so other people can manage time efficiently, projects don't slip through the cracks, and so that you don't get bothered while away.  
1. You should add the time off using [PTO Ninja](#pto-ninja) to Slack. Please use the following syntax which uses the [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) date notation in order to avoid confusion: `OOO from YYYY-MM-DD to YYYY-MM-DD, please contact XXX for assistance`.
    * PTO Ninja will automatically add this time to BambooHR and as a busy event on your Google Calendar. Your manager will also receive a free event on their calendar.  
1. Add an out of office automated response including the dates you plan to be away in your automated response. 
1. If you plan to be out of the office for more than 48 hours, update your GitLab.com status with your out of office dates by clicking on your profile picture and selecting "Edit Status." For Example: 'OOO Back on 2018-06-28.' Don't forget to change it back upon your return, and be aware that this information is publicly accessible.
    * Adding `OOO` to your status message will keep you from appearing in the [reviewer roulette](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette).
1. If your team or work group has a specific scheduling calendar, ensure to update it with your out of office plans.
1. Decline any meetings you will not be present for so the organizer can make appropriate arrangements.
    * Cancel, move, or find coverage for any meetings for which you are the organizer.
    * Optionally, if you partake in [Donut coffee chat pairings](/company/culture/all-remote/tips/#coffee-chats) you can temporarily “snooze” them by opening a direct message with Donut and typing `help` to indicate which weeks you won't be able to participate. Pairings will automatically resume when you’re back.
1. If you manage a large team, it may be useful to add your planned time off as a **FYI** on the next agenda of the company call.
1. If you are an interviewer, review your calendar to address any scheduled interviews. To ensure we provide a great candidate experience, if you find that you cannot attend an interview, you will be responsible for finding a replacement interviewer. You must communicate directly with your recruiter, as being out of the office does not always mean that you will be unavailable to interview.
1. If you're one of the people who typically handle availability emergencies (the on-call heroes), you **do** need to ensure that someone will be available to cover for you while you're out of office. You can check for this with your manager. Managers can import their team's calendars into their Google Calendar to get a quick view of their team members' availability.
1. Being part of a global remote team means you need to be highly organized and a considerate team player. Each team has busy times so it is always a good idea to check with them to ensure there is adequate coverage in place.
1. Please see the [On-Call](/handbook/on-call/) page for information on how to handle scheduled leave for someone from the On-Call team.
1. If you are a manager, please be sure to delegate the expense approval to your back up team member in Expensify:
   * Expensify
   * Settings
   * Your Account
   * Vacation Delegate - enter their email address and Payroll will grant the backup team member the necessary access
   * Note - remove the vacation delegate after your PTO

### Communicating Time Off for an Emergency Situations
Emergencies, by definition are unexpected.  They can range from natural disasters, terrorist events, accidents, family deaths, hospitalization and any other unexpected situation.  During these times we ask team members to use their best judgement as well as listen to and adhere to public safety officials when possible.  If an unexpected emergency occurs please contact your manager via slack or email as soon as possible if you will be unavailable or unable to work.  This will allow your manager to confirm your safety and reassign any critical work during your absence.  If you do not have an emergency contact in BambooHR please go ahead and complete that section.  We will only contact that person if we are unable to reach you via slack, email or phone.  

### Unpaid Leave of Absence
Team members may request an unpaid leave of absence by working with their manager to obtain approval. A manager has the discretion to not approve a leave based on performance issues or critical deadlines.  All requests must be approved by the function Vice President and comply with all local laws. Please follow the process for initiating an unpaid leave request to Peopleops.
* Once an unpaid leave request has been approved by the team members manager and functional Vice President the manager should send an email to Peopleops that will include the team members name, location, start and end date of approved unpaid leave.  Please copy your functional HRBP on the unpaid leave request email.
* Once Peopleops receives the unpaid leave email request they will confirm the manager and VP approval with the functional HRBP.
* Once Peopleops confirms with the HRBP, Peopleops will inform payroll via email of the approved request.  The email will contain the team member name, location, start and end date of unpaid leave.
* Peopleops will confirm with the team member via email that the request has been approved including the start date and end date of the unpaid leave.
* The team member should contact their manager 2 weeks prior to the unpaid leave of absence ending to confirm the return to work date.  The team member may ask for additional unpaid time off, however the manager has the discretion to deny any additional time off requests.  
* At anytime during the unpaid leave the team member receives a payroll payment, they are to immediately notify their manager and Peopleops via email.  Any funds paid in error are subject to repayment. 


### PTO Ninja

[PTO Ninja](https://treehoppr.com/pto-ninja) allows employees and managers to coordinate time off seamlessly with intuitive Slack commands. The integration from Slack and BambooHR automatically monitors PTO balances and takes action. PTO Ninja also reminds employees of their upcoming time off and helps them assign roles and tasks for co-workers, giving all parties involved greater alignment and peace of mind. 

[PTO Ninja Training Video](https://drive.google.com/file/d/1BYzJ4B4fSno-yyxwSPG8V4AV4Q8XhbnT/view?usp=sharing)

Please be aware that new team members' PTO Ninja accounts are activated and synched automatically once a day, so if you try to access PTO Ninja on your first day and receive an error message, please attempt to access PTO Ninja on Day 2. If you still receive an error message, let your assigned People Ops Specialist know in your onboarding issue.

**Slack Commands:**
* `/ninja ooo` Create an OOO event.
* `/ninja me` View your OOO dashboard to edit, add or remove OOO events.
* `/ninja whosout` See upcoming OOO for everyone in the channel where this command is used.
* `/ninja @username`  Check if a particular person is OOO and if they are, see which of your co-workers are covering for them.
* `/ninja feedback` This is your direct line to support. At any time, use this command to report bugs or share your
thoughts on how the product can be improved or what’s working well.

**Additional Features:**
* Google Calendar Sync: PTO Ninja allows you to sync time off requests with your Google Calendar.
* Automatic Status + Do Not Disturb Updates: PTO Ninja can automatically set your OOO status in Slack and apply “Do Not Disturb” settings while you’re away. You must add these permissions individually.
* Roles and Task Handoffs: PTO Ninja provides an experience that allows you to set roles and tasks for co-workers while
you’re away. Accepted roles then become a part of a Slack away message that appears as
co-workers try to tag you in discussions while you’re OOO.

### Management’s Role in Paid Time Off

Managers have a duty of care towards their direct reports in managing their wellbeing and ensuring that time off is being taken. Sometimes, when working remotely from home, a good work-life balance can be difficult to find. It is also easy to forget that your team is working across multiple time zones, so some may feel obligated to work longer to ensure there is overlap. It is important that you check-in with your reports through one-to-ones, and if you think someone needs some time off let them know they can do this.

If you discover that multiple people in your team want to be off at the same time, see what the priorities are, review the impact to the business, and use your best judgement. Discuss this with your team so you can manage the time off together. It is also a good idea to remind your team to give everyone an early heads-up, if possible, about upcoming vacation plans.

### Recognizing Burnout

It is important for us to take a step back to recognize and acknowledge the feeling of being "burned out".
We are not as effective or efficient when we work long hours, miss meals or forego nurturing our personal lives
for sustained periods of time. If you feel that you or someone on your team may be experiencing [burnout](http://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/burnout/art-20046642), be sure to address it right away.

To get ahead of a problem, be sure to communicate with your manager if any of the following statements ever apply to you:

* "I am losing interest in social interaction." - This is especially dangerous in an all-remote setting.
* "I've lost the motivation to work." - Everyone has days when they don't want to work
but if you hear yourself saying this often, you're on the road to burnout.
* "I often feel tired." - Indicative of being overworked for prolonged periods of time.
* "I get agitated easily."
* "I've been hostile to my coworkers." - You see yourself "snap" at people for
no apparent reason.
* "I've been having headaches often." - A headache can manifest itself for multiple
reasons but if you catch yourself only having headaches on work days, it is time to
evaluate your situation.

If someone is showing signs of burnout, they should take time off to focus on things that are relaxing and improve their overall health and welfare.

As a manager, it is your task to evaluate your team's state of mind.
Address possible burnout by discussing options with your team member to manage contributing stressors and evaluate the workload.
Some things to help with this:

* Try to follow each of your team members' work habits. If they start being less efficient,
or working more hours, they might be on the road to burnout.
* Try to keep track of when they had their last paid day off. If they hadn't had a personal day
in a long time, look closer at their behaviour.
* Make sure you let your team members know they can talk to you about their challenges.
* When you recognize symptoms of burnout in others, help them to get out the "Burnout trap".
Don't just tell people to [take a break](https://medium.com/@zenpeacekeeper/take-a-break-f877907877bc),
but help them arrange things so they **can** take a break. Ask why they feel they can't take a break
(there are almost certainly real, concrete reasons) and then ask permission to get busy putting things in place
that will overcome those barriers. People might be trapped by their own fatigue, being too worn out to find the creative solutions needed to take a break.

Other [tips to avoid burnout](http://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/burnout/art-20046642?pg=2) include:
* Make use of our [Employee Assistance Program](/handbook/benefits/#employee-assistance-program) that can offer professional support during this time.
* Assess and pursue your interests, skills and passions.
* Take breaks during the day to eat healthy food and stretch your legs. The [Timeout app](http://dejal.com/timeout/) can help with that.
* Make time each day to increase blood and oxygen circulation which improves brain activity and functionality.
* Get plenty of restful sleep.
* Meditate to take your mind away from work. [Headspace](https://www.headspace.com/science) and [Calm](https://www.calm.com/meditate) are good tools for creating meditation habits.
* Don't start work as soon as you wake up. Take your time doing your morning routine.
* Set yourself as away when you are not working. Snooze your Slack notifications. It is
fine to be not reachable during your off time.

Don't let burnout creep up on you. Working remotely can allow us to create bad habits, such as working straight through lunch to get something finished. Once in a while this feels good, perhaps to check that nagging task or big project off the list, but don't let this become a bad habit. Before long, you'll begin to feel the effects on your body and see it in your work.

Keep in mind that you are not alone! Chances are that you have a colleague who already
experienced burnout or has been on the road to burnout. Schedule coffee calls with
your team members or with anyone you'd like to talk to. Talk to your manager. If none of that
is an option for you, schedule a coffee call with [Marin](slack://user?team=T02592416&id=U025925R0).

Take care not to burn yourself out!

### Statutory Vacation Requirements

The following is a list of all statutory annual vacation entitlements by entity and country. Once the statutory amounts have been taken, employees can still make use of GitLab's unlimited leave policy.

1. GitLab LTD (UK Employees)
    * Employees are entitled to at least 20 vacation days. The days will accrue from the start date. There is no carryover for unused vacation days.
1. GitLab BV (Netherlands Employees)
    * Employees are entitled to at least 20 vacation days. The days will accrue from the start date. Any unused days will be carried into the next calendar year, but expire after six months.
1. GitLab BV (Belgium Employees)
    * Employees are entitled to at least 20 vacation days; the days taken must be communicated to the Belgian payroll provider each month by People Ops. These days do not carry over into the next calendar year.
1. GitLab BV (Contractors)
    * Contractors do not have statutory vacation requirements, but are eligible for our Unlimited Time off Policy.
1. GitLab GmbH (Germany Employees)
    * Employees are entitled to at least 20 vacation days. The days will accrue from the start date. In general, employees must take their annual vacation days during the calendar year, otherwise it is forfeited. However, unused vacation days can be carried forward until the 31st of March of the next calendar year if the employee was unable to take the holiday due to operational or personal reasons.
1. GitLab INC (US Employees)
    * The U.S. [Fair Labor Standards Act (FLSA)](https://www.dol.gov/general/topic/workhours/vacation_leave) does not require payment for time not worked, such as vacations, sick leave or federal or other holidays.
1. GitLab Inc. (China)
    * For employees who worked for one-year but less than 10 years, the annual leave is 5 days; for more than 10 years worked but less than 20 years, the annual leave is 10 days; for more than 20 years worked, the annual leave is 15 days.
1. LYRA (India)
    * The statutory requirements of India are covered through our Unlimited Time off Policy.

### Processing Vacation Requirements

1. Each June, People Ops will need to review the BV (Netherlands) Accrual for all employees and remove any carry over from the previous calendar year that was not used.
1. In January, People Ops will adjust any negative carryover back to zero, for all accruals in BambooHR for the year. To do this go to Time Off, Adjust Balance, and add any extra days.
1. PTO Ninja will automatically add any time off taken for sick and vacation accruals.  
