---
layout: handbook-page-toc
title: "Secure UX"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
We’re designing an experience that enables contributors to commit their most secure work. This is done by merging security into the DevOps process, giving development teams more ownership, commonly referred to as DevSecOps. The experience brings cross-functional stakeholders together to make better, faster, and more secure-oriented decisions. We are doing this by focusing the experience on automation, education, empowerment, and shifting security to the left.

**Automation** relates to [convention over configuration](/handbook/product/#convention-over-configuration) that helps draw a clear path for the user to produce meaningful results. When it comes to web security, no application will ever be 100% secure. That’s why we are focused on integrating automation into every step of the user’s journey, taking the guesswork out of configuration to open up more time on what’s important: resolving vulnerabilities.

**Education** for our users so they understand security basics and are aware of security needs in their applications. We want our users to know where vulnerabilities have been detected, visualize the implications, present resources to understand the problem, and provide the tools to facilitate informed decisions about next steps.

**Empowerment** for all users to resolve security issues is essential as cross-functional departments share ownership of security. Our tools strive for an experience where the developer is *responsible* and the security team is *accountable* for the organization's security.

**Shifting left** is taking things like QA and other processes typically found later in the ops cycle and moving them to development. Resulting in security problems being addressed early and often.

### Our customer
Organizations of all sizes benefit from our tool and the experience of bringing teams together. We provide customers value with workflow efficiency, informed team decision-making, lower risk of security breaches, and attaining compliance requirements. We focus on all aspects of the product — starting with the customer experience. When deciding to use our tool, organizations are often considering the following:

* What languages does the tool support?
* What tests do we need to cover?
* What tests does the tool cover?
* Can it be automated?
* How long will setup take?
* What does setup involve?
* How easy is it to use?
* What technologies do you need to use? ex. Docker, Kubernetes
* How lightweight is the tool?
* How does it integrate with our tools?
* What customer support is offered?
* What are the upcoming features? (we are selling contracted services vs monthly)


### Our user
We have different user types we consider in our experience design effort. Even when a user has the same title, their responsibilities may vary by organization size, department, org structure, and role. Here are some of the people we are serving:

* [Software Developer](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
* [Development Tech Lead](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
* [DevOps Engineer](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
* [Security Analyst](/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst)
* Compliance Associate
* Security Engineer
* Chief Information Security Officer

Generally, developers are the users of the vulnerability reports in the MR/pipeline while security professionals are the users of the Security Dashboards.

### Our UX Scorecards
##### Primary Jobs to be done (JTBD)
* **Interacting with vulnerabilities in the MR:** When committing changes to my project, I want to be made aware if I am adding risk through vulnerable code, so that I know my changes can be merged without increasing the risk of my project.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/400)
     * Recent recommendation: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/479)
* **Interacting with vulnerabilities in the security dashboard:** When reviewing vulnerabilities for multiple projects, I want to see them all in one location, so that I can prioritize my efforts to resolve or tirage them while seeing the larger picture..
     * Current walkthrough:  [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/401)
     * Recent recommendation: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/460)
* **Managing licenses (user that is responsible):** When new licenses are added to a project I want to be aware so I can commit work that is compliant with my organization's rules.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/478)
     * Recent recommendation:[view issue](https://gitlab.com/groups/gitlab-org/-/epics/1618)
* **Managing licenses (user that is accountable):** When my organization has license compliance rules to follow I want to be able to whitelist or blacklist licenses so that I can ensure any new code merged in a project is in compliance.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/402)
     * Recent recommendation:[view issue](https://gitlab.com/groups/gitlab-org/-/epics/1618)
* **Vulnerability check (user that is responsible):** When a merge request is disallowed, I want to know why, so I can resolve the issue and proceed with the MR.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/534)
     * Recent recommendation: *in progress*
* **Vulnerability check (user that is accountable):** When new vulnerabilities are detected in a merge request, I want to disallow the merge request, so the team can review the vulnerabilities to resolve or decide on the next steps.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/533)
     * Recent recommendation: *in progress*
* **Configuring Secure features:** 1. When I want to configure my security tools, I want to be able to configure them to address my own business risk policies, so that I can be assured my company is monitoring risk based on our business risk policies. 2. When I want to implement security tools, I want to be able to install them easily and know they are working properly, so that I can be reassured my company is managing and monitoring risk.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/592)
     * Recent recommendation: *in progress*
* **Dependency list (responsibility):** When my organization has a compliance policy with dependencies, I want to be aware if I’m breaking a company policy, so I can make sure my project dependencies are in compliance with my org compliance.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/584)
     * Recent recommendation: *in progress*
* **Dependency list (accountability):** When I need to audit 3rd party licenses and dependencies, I want to be able to view them, so I can have them on record for auditing purposes and be able to share them with auditors and customers.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/583)
     * Recent recommendation: *in progress*


### Our team
Our team continues to grow. We currently have 7 members that contribute to Secure UX efforts:

* [Valerie Karnes](https://gitlab.com/vkarnes) - UX Manager
* [Tali Lavi](https://gitlab.com/tlavi) - UX Researcher
* [Kyle Mann](https://gitlab.com/kmann) - Sr. Product Designer, Software Composition Analysis, Compliance & Auditing
* [Camellia Yang](https://gitlab.com/cam.x) - Sr. Product Designer, Static and Dynamic Testing, Security Testing
* [Annabel Dunstone Gray](https://gitlab.com/annabeldunstone) - Product Designer, Static and Dynamic Testing, Code Scanning
* [Andy Volpe](https://gitlab.com/andyvolpe) (transitioning to Defend) - Sr. Product Designer
* [Becka Lippert](https://gitlab.com/beckalippert) (transitioning to Defend)- Product Designer

Our team meetings:
* Secure UX weekly meeting on Wednesdays at 10:30am EST. Meeting to discuss our stages UX shared efforts, review designs, and iterate on our strategy.
* Product Design and Secure PMs bi-weekly on Wednesdays at 10:00am EST
* Product Design does a bi-weekly grooming session on Tuesdays at 10:30am EST

### Our Structure
We've divided the Secure stage into dedicated experience groups to align with a similar [split](/handbook/product/categories/#secure-section) undertaken by our engineering and PM counterparts.

**Static and Dynamic Testing**

| Experience Group | Features                                                     | Designer(s)           |
| ---------------- | ------------------------------------------------------------ | --------------------- |
| Security Testing | SAST, IAST, DAST, Fuzzing, Container Scanning, Secret Detection | Camellia Yang         |
| Code Scanning    | Scanning in Web IDE, MR Security Report, DevSec Code Review, Dependency Scanning | Annabel Dunstone Gray |

**Software Composition Analysis**

| Experience Group            | Features                                                     | Designer(s)         |
| --------------------------- | ------------------------------------------------------------ | ------------------- |
| Compliance & Auditing       | License Check, Security Gates, License Compliance, Bill of Materials, Auto-Remediate, Dependency Scanning | Kyle Mann           |
| Audits & Policy Enforcement | Audit reports, SLAs, Risk Acceptance, Policy Enforcement     | TBD - we're hiring! |

The Secure & Defend UX teams work closely together and have shared coverage in the following areas:

- Vulnerability Management
- Security Dashboard
- Control Center
- Status, Metrics and Reporting
- Security Configuration

This segmentation gives us a better opportunity to:
- Grow our expertise and knowledge within our subgroup while sharing relevant information with the rest of the team.
- Evolve and maintain relationships with our dedicated engineering team and PMs.
- Serve as the known main point of contact.
- Deeply understand our users' needs by initiating and/or leading research activities specific to our experience group.
- Focus on iterating and progressing our experiences from MVC to Lovable.

#### How we work

##### Labels we use

We created 3 scoped labels to help us identify which experience group a particular issue falls into and which designer should be subsequently assigned. 

`Secure UX::Shared`
- Issues relating to Vulnerability management • Status, Reporting & Metrics • IA and Core Functionality.
- Examples: Adding Secure to the left nav, Inline vulnerability management, Dashboard designs.
- Who to ping: If no designer is assigned: Both designers. If a designer is assigned, ping them in the issue.

`Secure UX::Security Testing & Scanning`
- Issues relating to security testing, scanning, and detection of vulnerabilities or weaknesses.
- Examples: MR Widget security report design, Secret detection, DAST - list of scanned URLs in MR.
- Who to ping: Camellia Yang

`Secure UX::Compliance & Auditing`
- Issues relating to features that support Compliance and/or Auditing activities.
- Examples, Security gates, License Compliance, Dependency list.
- Who to ping: Kyle Mann

##### Problem and solution validation issues 

When working on a `workflow::problem validation` or `workflow:solution validation` issue requiring implementation in the next 2 releases, ensure there is a placeholder implementation issue. This issue must be attached to the epic, have a tentative milestone and the corresponding labels, particularly the group label, so that it shows up on the issue boards for our counterparts.

##### Meetings we host

We host a recurring 30-minute open meeting every week to discuss topics relevant to Secure design, UX, and research. Some example topics could include: 

- Updates on in-flight and planned research
- Updates on design issues
- Issues that might be at risk or has blockers
- Recently discovered insights while conducting researching
- Updates to our Baseline initiatives
- Updates on changes to UX workflows and processes
- Updates on pilot initiatives we are working on

Some topics are better suited for a dedicated meeting, and should be created on an as-needed basis:

- Milestone planning and grooming
- Design critiques 
- Research report readouts 
- Syncing on troublesome issues 

##### Planning and grooming

Secure UX has a separate grooming session which takes place during the planning phase of a milestone. During grooming, we add the proper label to all issues requiring UX support.

Read more about how we've created these dedicated experience groups [here.](https://gitlab.com/gitlab-org/gitlab-design/issues/458)


### Our strategy
The Secure UX team is working together to [uncover customers core needs](https://gitlab.com/groups/gitlab-org/-/epics/1611), what our users’ workflows looks like, and defining how we can make our users tasks easier. Our strategy involves the following actions:

* [UX Scorecards and recommendations](/handbook/engineering/ux/ux-scorecards/) (quarterly)
* Internal understanding: stakeholder interviews (annually)
* Iterating on Secure product [foundation's document](https://gitlab.com/gitlab-org/gitlab-design/issues/333) (ongoing)
* Performing heuristic evaluations on at least 3 competitors, based competitors the 3 user type is using (annually, ongoing)
* We talk to our customer (ongoing)
* We talk to our users (ongoing)
* We outline current user workflows and improve them (upcoming, ongoing)

Additionally, we value the following:
* Testing our features with usefulness and usability studies
* Drinking our own wine and partnering closely with our internal Security and Compliance teams for feedback and feature adoption
* Partnering with our sales and account team to connect directly with customers and learn why customers did (or didn’t) choose our product
* Collaborating with stakeholders on proof of concept discoveries to better understand future consideration
* Prioritizing issues that are likely to increase our number of active users

The source of truth lives with shipped features, therefore we:
* Make data-driven decisions quickly and thoughtfully
* Optimize to deliver our solutions as soon as possible
* Learn, iterate, test, and repeat

### Follow our work
Our [Secure and Defend UX YouTube channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KrFCe5BgUkzFrZifjforQOz) includes UX Scorecard walkthroughs, UX reviews, group feedback sessions, team meetings, and more.