---
layout: handbook-page-toc
title: "Executive Business Administrators"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro

This page details processes and general guidelines specific to the Executive Business Administrators (EBA's) at GitLab. The page is intended to be helpful, feel free to deviate from it and update this page if you think it makes sense.

## Executive Business Administrator Team 

* Cheri Holmes (Manager of EBA Team) supporting Sid Sijbrandij (Co-Founder & CEO) and Michael McBride (CRO)
* Stefanie Haynes (Sr. EBA) supporting Todd Barr (CMO)
* Jaclyn Grant (Sr. EBA) supporting Paul Machle, CFO
* Vanessa Wheeler (EBA) supporting Scott Williamson (VP of Product), Mark Pundsack (VP of Product Strategy) and Eric Johnson (VP Engineering)
* Trevor Knudsen (EBA) supporting Sung Hae Kim (CPO), Dave Gilbert (VPR), and Carol Teskey (Director, Global People Ops)

## Meeting request requirements

If you would like to schedule a meeting with E-Group at GitLab, email or slack the EBA with the following information:
* Must have/optional attendees 
* Meeting type: internal prep, client facing/customer, prospective customer, etc
* Urgency/ Desired timeframe: in the next two days, in the next week or two, etc
* Duration: 25 mins, 50 mins, etc.
* Subject of the meeting
* Provide context: include agenda to be covered. Share google doc if available to be included in invite or link relevant issues, slides, etc.
* [Meeting Template](https://docs.google.com/document/d/1qj4MRlIXXGs4Jni0ITYp1uaHDQr53IvOqmpN-47RD-k/edit#heading=h.954v91mukl7r) should be used for all meetings with members of our E-Group and provided at the time of the meeting request. Select file > make a copy to use this template.
* [Customer Briefing Document](https://docs.google.com/document/d/1hyA12EN5iEwApAr_g4_-vhUQZohKxm5xkX9xxZ1JNog/edit?usp=sharing) should be used for all meetings with Customers or Prospects and provided to the EA at the time of the meeting request.  Select file > make a copy to use this template and please make sure it is editable by everyone at GitLab. Watch Nico Ochoa in [this 6.5-minute video](https://youtu.be/Wdg9YGQvs20) share his best practices for completing this template ([click here](https://docs.google.com/document/d/1rITvwVvv9Vn89qAgRX2Pf69zzoRp2AcqjtYBVUAria8/edit?usp=sharing) for Nico's template). If you have any questions related to this document, please ping in #eba-team in slack. 
* If you have an urgent meeting request that is customer related, please provide the EBA with all of the above and a high level agenda if you do not have time to complete the briefing document in advance or if we are vetting prospect meetings with our E-Group.  
* If you have a request for a member of E-Group to attend and speak at an event (keynote, panel, fireside chat, etc.), please fill out the [Executive External Event Brief](https://docs.google.com/document/d/1qMCw9gGTAdk3YW-AW2W_eCYXMGcNYyaq_wN0giSf-yk/edit) with the key information and email to the EBA supporting the Executive with your request. The EBA will review, provide feedback, and gain approval accordingly and confirm directly with you. If the request involves the CEO, please follow the process outlined here. The EBA to the CEO will gain approval from the function head of the requestor on whether the CEO will attend or not. Whether a new talk needs to be prepared or an existing one can be adjusted to the audience should be a part of the decision whether or not to participate in the event. 

In the event that you have a **same day** request, you can send the above information in the "eba-team" Slack channel and @ mention the appropriate EBA. 

## EBA Team Best Practices 

### Suggested formats for calendar invites within Executive Group (E-Group)
* MTG for meetings in person, either at Mission Control or another location
* INTERVIEW for media interviews (make sure to loop in our PR partner)
* CALL for phone calls or conference calls 
* VIDEOCALL for video conference calls using Zoom
  * Example: “VIDEOCALL Jane Doe for Executive Assistant” or "VIDEOCALL Jane Doe (GitLab) & Sid Sijbrandij (GitLab)""
* LIVESTREAM for livestreams
  * When a meeting is being live-streamed to YouTube add Public Steam or Private Stream in the subject and body of the calendar invite to indicate which live stream
* A candidate interview will include the work interview using regular casing. 
* 1:1 for one-on-ones with direct reports
  * Example: "1:1 Jane Doe & Sid" 
* "Skip Level" should be in the title of skip level meetings
* Flight travel should include "flight" in the title
* Ground transportation is indicated by "Uber", "Ground Transportation", or "Car service" in the title
* Executive time should be labeled "Exec Time" or "Executive Time"
* Please utilize the [Zoom plugin for Google Calendar](/handbook/communication/#video-calls) to schedule Zoom meetings on behalf of E-Group
* Please add the subject of the call in the description, for internal and external calls.
* All external meetings RSVP should be confirmed with the guests at least 24hrs in advance 
* When meetings are being rescheduled please add RESCHEDULING in the subject line of the calendar invite to indicate the reschedule and change the RSVP to "No"


Make sure to include the following items (which are detailed below) in the calendar invite:

1. Calendar blurb
1. Cell numbers (in case of an in-person meeting)
1. Mission Control Access Instructions (when meeting at Mission Control)

### General scheduling guidelines

* For meetings spanning across multiple time zones and with external parties, [Time & Date Calculator](https://www.timeanddate.com/worldclock/meeting.html) can help determine the best time to schedule
* When scheduling internally with multiple attendee's, please utilize Google [calendars](https://calendar.google.com)[Find a Time feature](https://gsuitetips.com/tips/calendar/find-a-time-in-google-calendar/) to see when GitLab team-members are free to schedule a meeting with. Please be cognizant of people time zones. If in doubt, please reach out to the team member directly in slack
* When researching flights, consider using a tool like [Skyscanner](https://www.skyscanner.com) or [Google Flights](https://www.google.com/flights?hl=en) to explore options for flights 
* Schedule calls in European timezones in the morning (am) Pacific (Daylight) Time and US time zones in the afternoon (pm) Pacific (Daylight) Time
* Holds on the schedule should be removed at least 60 minutes before the preceding meeting starts. Preferably 24hr in advance of the meeting when possible
* Meetings should be scheduled for 25 minutes or 50 minutes.  As a general guideline meetings should not be scheduled to the full 30/60 minute mark
* If the call is with any Google company, use Hangouts instead of Zoom.
* For meetings at Mission Control and another guest joining via video call: The EBA will schedule an additional ten minutes before the appointment to test the zoom room system.
* For meetings or lunch/dinner appointments, always make sure to add the address in the invite of the location where it’s scheduled.
* Make sure to plan travel time (in a separate calendar item, just for the exec) before and after the meeting in case another meeting or call should follow.
* After each meeting with a potential investor, make sure to update Airtable with the information on these meetings.

### Travel

EBAs research the best option for a flight and book it according to their executive's schedule. If the Executive requires approving the flights beforehand, please follow that process before booking.
Make sure to add a calendar item for 2 hours before take off for check in and add a separate one for travel time before that in the exec's calendar.
If a flight was changed or not taken due to delays or other circumstances, make sure to check with the airline for the current flight status.

### Expensify

* When you’re logged in, you can find wingman account access for other team members in the top right corner menu.
* Check their email (if you have access), using the search bar in the top, to find any receipts for the postings in the current expense report.
* And/or write down what receipts are missing and email to request them if needed.

### E-Group In-person Meetings

There should be one invite for all attendees that includes the following:

* Exact meeting time blocked (ie: Start at 9am PST, End at 5pm PST)
* Zoom Link for remote participants
* Agenda (the agenda should also include the zoom link at the top)
* Notes doc shared via calendar invite and sharing set to "can edit" for those attending the meeting

## Public engagement

The public process does two things: allows others to benefit from the conversation and it acts as a filter since there is only a limited amount of time so we should prioritize conversations that a wider audience can benefit from.

### OKRs 
* EBA to the CEO to assist in maintaining and scheduling meetings revolving around the [OKR updating process](/company/okrs/#updating).

## Performance Indicators (PI) 

### Leadership SAT Survey

On a quarterly basis, any Executive at GitLab that has dedicated administrative support via an Executive Business Administrator will receieve a survey with a series of questions and a rating scale of 1-5 (5 being excellent) to determine the performance of the EBA team. 

Sample Questions: 
* Tasks are completed within agreed timeframes, to accurately meet requirements and consistent with GitLab’s policies and procedures.
* EBA responds to queries in a timely manner based on the urgency of the request
* EBA knows and keeps Executive(s) informed of all activities and appointments at all times.
* EBA interacts and communicates clearly with key stakeholders both internally and externally through completion of tasks with little direction
* EBA proactively seeks feedback on the quality of administrative support and demonstrates an approach of continuous improvement


## Scheduling preferences for Sid Sijbrandij, Co-founder and CEO

* Don't schedule over the Weekly E-group call unless approved by Sid
* When our VP of Legal requests a meeting it will always override other meetings that are scheduled if urgent
* Mark the events listed in [Google Calendar section](/handbook/communication/#google-calendar) of the handbook as 'Private'
* The [agenda](https://docs.google.com/document/d/187Q355Q4IvrJ-uayVamoQmh0aXZ6eixAOE90jZspAY4/edit?ts=574610db&pli=1) of items to be handled by Sid's EA
* **All** holds on Sid's calendar need a reason so he can judge when the hold might no longer apply.
* Monthly video calls are 25 minutes while quarterly calls/dinners are scheduled for 90 minutes plus any necessary travel time.
* After each meeting with a potential investor, make sure to update Airtable with the information on these meetings.
* Follow up on introductions from certain recipients (board, investors) immediately without asking for approval.
* If Sid is meeting with a candidate, partner with recruiting to send the calendar invite through Greenhouse.
* If Sid has an external meeting and the location is less than 25 minutes away on foot, he would prefer to walk. Please ensure the calendar reflects that he is walking versus driving/uber/lyft.
* If Sid has a **ride or walks** to an appointment, make sure to **add 5 minutes extra** to find the address and sign in at reception.
* If Sid is **driving himself**, make sure to **add 15 minutes extra** for random occurrences such as traffic, stopping for gas or parking.
* If Sid is **driving himself** to a meeting, he likes to plan phone calls to catch up with the team. Check with him who he'd like to talk with during his commute and schedule accordingly or post in #ceo channel in slack offering a call with Sid our team members.
* Due to a busy schedule Sid has a preference of meeting setup: First try for a video call or a meeting at Mission Control. If the other party presses to meet at their location, confirm if that is OK before accepting.
* Sales meetings are important. If the CEO can help the process, feel free to include him in the meeting by working with the EBA to CEO on this. Please include the sales rep and solutions architect in this meeting. The person requesting the meeting should provide a meeting brief document to the EBA at the time of their request to schedule.

### CEO Email Management

* Labels: `/archive`, `/read-respond`, `/personal` or `/urgent-important`
* Prepare draft responses
* Proactively schedule meetings requested via e-mail
* Standard reply for recruiters:
“We do not accept solicitations by recruiters, recruiting agencies, headhunters, and outsourcing organizations. Please find all info [on our jobs page](/jobs/#no-recruiters)


### Travel preferences
Current preferences for flights are:
* Aisle seat
* Check a bag for all trips longer than one night
* Frequent Flyer details of all (previously flown) airlines are in EA vault of 1Password as well as important passport/visa info

### Pick Your Brain meetings

If people want advice on open source, remote work, or other things related to GitLab we'll consider that. If Sid approves of the request we suggest the following since we want to make sure the content is radiated as widely as possible. To start, we send an email as shown below. 

> Thanks for being interested in GitLab. If we schedule a meeting it will follow GitLab's [Pick Your Brain format](/handbook/ceo/#pick-your-brain-meetings). Are you able to submit a draft post with us within 48 hours of interview? 
> 
> GitLab is a very transparent company and many things that are normally confidential can be found in our handbook, available online. Please consider looking at the following pages prior to our meeting.
>   * [Company strategy including planned going public date](/company/strategy/) 
>   * [Our Objectives and Key Results per quarter](/company/okrs/)
>   * [All team members and numbers per department](/company/team/) 
>   * [Handbook of 3000 pages with all our processes](/handbook/) 
>   * [Pricing plans](/pricing/) 
>   * [Pricing strategy](/handbook/ceo/pricing/) 
>   * [History](/company/history/)
>   * [About](/company/)


1. If we receive a positive answer we schedule a 50 minute YouTube public livestream. For an example of an interview, see [this one about stress in remote work](https://www.youtube.com/watch?v=23XIx6n9SsQ).
1. Within 48 hours you share a draft post with us in a Google Doc with suggestion or edit rights for anyone that knows the URL.
1. You can redact anything you don't want to publish.
1. Our executive administrator will work with you to publish the post if we agree that it fits with our current marketing plan and would be a good fit for our audience. The EBA-team will follow up to make sure the draft post is submitted and coordinate with Content Marketing to cross-post on our blog.
1. Read and watch [past Pick Your Brain interviews](/company/culture/all-remote/interviews/#pick-your-brain-interviews), including [one hosted by Slab co-founder Jason Chen](/blog/2016/07/14/building-an-open-source-company-interview-with-gitlabs-ceo/) that reached [the top spot on Hacker News](https://news.ycombinator.com/item?id=12615723).

Reply to emails: Thanks for wanting to chat. I propose we meet in the format proposed in GitLab's [Handbook](#pick-your-brain-meetings) so that other people benefit from our conversation too. If you're up for that please work with Cheri (cc:) to schedule.


### Scheduling Pick Your Brain meetings

1. Once Pick Your Brain meetings are confirmed, schedule a Zoom Webinar and send out a calendar invite to all guests and make sure to add a separate calendar invite for Sid with preparation time. [Scheduling a Zoom webinar](https://support.zoom.us/hc/en-us/articles/115000350446-Streaming-a-Webinar-on-YouTube-Live)
1. Link the document that the external guest has provided to the calendar invite and ensure it also includes:
* Who the PYB Meeting is with, Name, Title, Company and their LinkedIn Profile (if applicable)
* Context: Where did the request originate from (email, text, twitter), what's the source?
* Subject: One Sentence for the PYB Meeting 
* If the PYB meeting is about all-remote work, please invite the Sr. Manager, All Remote Culture Curator to the meeting 

### Calendar blurb for External Meetings with the CEO

For all external parties meeting with CEO (both video call and in-person), the EBA should include the following verbiage and links in the calendar invite:

```
GitLab is a very transparent company and materials that are normally confidential can be found online, please consider looking at the following Handbook links prior to your meeting with GitLab:

Company strategy including planned going public date: https://about.gitlab.com/company/strategy/
Our Objectives and Key Results per quarter: https://about.gitlab.com/company/okrs/
All team members and numbers per department: https://about.gitlab.com/company/team/
Handbook with all our processes in: https://about.gitlab.com/handbook/
Pricing plans: https://about.gitlab.com/pricing/
Pricing strategy: https://about.gitlab.com/handbook/ceo/pricing/
History: https://about.gitlab.com/company/history/
About: https://about.gitlab.com/company/
GitLab Unfiltered on YouTube https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/videos 
```

Depending on the subject of the interview (e.g. [events](/events/), [all-remote](/company/culture/all-remote/), etc.), the arranger should consider adding other relevant links beyond the baseline below. 

If the topic of the media interview is solely or primarily about all-remote, suggest the opportunity to first speak with GitLab's all-remote culture curator.

Contact details for external meetings should be collected in advance by the EBA to the CEO and listed in the calendar invite as:

Executive cell:

Guest cell:

### Visiting Mission Control in San Francisco, CA

The EBA to the CEO will coordinate all meetings at Mission Control. If a group would like to visit GitLab's offices, please ping the EBA to the CEO in slack to discuss the opportunity. Due to limitations in size, we cannot host large groups but are willing to meet remotely and publicly. GitLab is an [all-remote and distributed organization](/company/culture/all-remote/) and we do not have a head office. 

Access to Mission Control: In case of a meeting in San Francisco at Mission Control please copy-paste building access instructions into the calendar invite from the "GitLab Mission Control Access Instructions" Google Doc.

## Scheduling for Michael McBride, Chief Revenue Officer

* Prefers “appropriate length” conversations, so short meetings are okay on the calendar.  If a topic only needs 10 mins, book 10 mins instead of 30, etc.
* Include Meeting Agendas in invites / make sure the team knows to include this with requests for time.
* Flexible with late evening calls for Asia or Australia - check with him first.
* Add pre-emptive blocks in calendar that can be used for meetings or calls.
* Schedule three 30 minute blocks a day for work time - title “workflow”. This is time for email follow-up and general work time. If the time has to move to accommodate another meeting ensure another time is found and that it can still happen.


## Scheduling for Paul Machle, Chief Financial Officer

* Priority to candidate interviews, 1:1's, E-Group, GitLab BoD and investor relations 
* Weekly finance team meeting and monthly all hands - EBA to CFO to help drive agenda 
* Prefers 25 minute video chats when possible. 15 min coffee chats are appropriate
* Unavailable due to personal commitments from 7-8am PT
* When in-person meetings are being requested, please check with the CFO
* Working blocks on the calendar are OK and can be moved to prioritize other meeting requests, team members should check in with the CFO's Sr. EBA to request a meeting using the meeting request requirements as a guideline


## Scheduling for Eric Johnson, VP of Engineering

* 1:1 Meeting title format: “Person:Eric 1:1” using [this template](https://docs.google.com/document/d/1vWm7-lmpqghoElckd02puqsKDNT6aCJInuZcfkdtvwQ/edit) 
* Tick the box “Attendees can modify”
* Please create an additional reminder (besides the default 10 min pop-up) that is 1 day. Email (reminds me to go into the doc and populate notes)
* All meetings should be set using a one-time meeting ID from Eric’s zoom room to avoid any accidental joins.  This includes interviews.  Note: Eric will attach private 1:1 docs to the meeting series once they are created.
* Eric will block off personal appointments and family related blocks.
* Add holds when scheduling meetings and interviews so he knows it's being worked on.
* If meetings need to happen before 8am PT or after 6pm PT check with him directly.
* Slack should be used for instant back and forth communication, not for items requiring followup or action 
* Email is the preferred method of contact for any items that need follow up, approvals or action by Eric
* Meeting requests for Eric should be posted in #eba-team  channel on Slack tagging EBA



## Scheduling for Todd Barr, Chief Marketing Officer 

* 1:1 Meeting title format: "Todd:Person 1:1"
* Color coding for calendar: 1:1's: blue, Reminders: yellow, Internal Meetings: light red, Marketing Specific Meetings: dark blue, Interviews: green, External Meetings: orange
* All 1:1 meetings should be scheduled on Mondays and Tuesdays
* Keep standing daily "blocks" that he has everyday for lunch and at the end of each day
* Block out open slots with "DNB-reach out to shaynes@"
* Create calendar reminders for GC's, AMA's and monthly key reviews two weeks in advance and then a final reminder one week prior
* Block off hour prep time before all 1:1's with CEO
* Keep Friday as open as possible for work time
* If meetings fall after 5pmEST, check with him directly
* Use personal Zoom links for interviews, all other meetings, especially external meetings, should utilize a one-time Zoom link

 

## Scheduling for Mark Pundsack, VP of Product Strategy


* Morning meeting times should be reserved for those outside of US time zones
* Mark does not mind others scheduling into his calendar, but meeting requests should be made so that it can be modified if necessary
* "Work time" should be preserved whenever possible
* "Blocked" times on the calendar coincide with personal time away from work, please confirm with Vanessa Wheeler prior to scheduling over this
* All meeting requests should include an agenda or doc for reference that should be provided to Vanessa Wheeler at least 48 hours prior to the scheduled meeting
* Meetings should be scheduled around the Group Conversations and Company Calls unless unavoidable.  If scheduling over these calls, please confirm in Slack #eba-team channel prior to sending an invite
* Working Sessions will be recorded and uploaded to GitLab's Unfiltered channel on YouTube within 24-hours (this can be private or public depending on the information discussed in the session)


## Scheduling for Scott Williamson, VP of Product

* All meeting requests should go through Vanessa Wheeler
* 1:1's with direct reports should occur weekly unless scheduling conflicts
* Meetings before 8:00am MDT and after 5:00pm MDT need to be confirmed prior to scheduling
* Weekly Product Meeting and Weekly Product Leadership meetings cannot be scheduled over
* Scott will allocate "DNB" blocks for work time based on upcoming priorities
* Green working blocks can be scheduled over
* Red blocks are either personal appointments or working blocks that cannot be compromised
* Reminders at -2 weeks, -1 week and -2 days should be put ont the calendar for the following events: OKR, Group Conversations (Product and Growth), Board of Director meetings, Key Monthly reviews
* 1:1's should always be rescheduled instead of cancelled unless unavoidable due to OOO/PTO schedules


## Scheduling for Sung Hae Kim, Chief People Officer 

* Ask before scheduling over personal events (Noted by purple color in calendar)
* Keep meetings to 25 or 50 minutes unless otherwise specified
* 1:1 meetings are light blue, internal meetings are red, external meetings are orange, coffee chats are dark green, reminders are in yellow, interviews are in dark blue. 
* Please change notifications to be 5 minutes.

## Scheduling for Dave Gilbert, VP of Recruiting

* Ask before scheduling over personal events (Noted by purple color in calendar)
* Keep meetings to 25 or 50 minutes unless otherwise specified
* 1:1 meetings are light blue, internal meetings are red, external meetings are orange, coffee chats are dark green, reminders are in yellow, interviews are in dark blue. 

## Scheduling for TBH, General Counsel

* To be updated by Sr. EBA to General Counsel
