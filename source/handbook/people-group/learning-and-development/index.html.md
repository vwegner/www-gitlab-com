---
layout: handbook-page-toc
title: Learning & Development
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Learning & Development (L&D) page at GitLab! L&D is an essential part of any organization's growth, success and overall business strategy. We want to support the growth of our GitLab team-members' competencies, skills and knowledge by providing them with the tools they need and also the opportunities to progress their own personal and professional development.  

## Learning Sessions

### Live Learning 
Live Learning sessions will be conducted on a monthly basis. There will be a Zoom video conference set up for each session. Official dates and topics will be added to the [schedule](/handbook/people-operations/learning-and-development/live-learning) as confirmed.

### Action Learning
Action Learning sessions are designed to give team members a place to practice their skills.

## Career Development

Everyone's career development is different, but we have [outlined](/handbook/people-group/learning-and-development/career-development/) what it can look like at GitLab. 

## Language Courses

If you have any language courses you would like to recommend or links to websites please add them to this section.

 - [The 8 Best Interactive Websites for Adults to Learn English](https://www.fluentu.com/blog/english/best-websites-to-learn-english/)

There is also a way to practice foreign languages and ask for advice in several Slack channels, each dedicated to a specific language. You can find all these channels by searching for channels starting with #lang. If you're missing a channel for your target language, feel free to create one and mention it in #company-announcements so that fellow GitLab team-members can join too!

## New Manager Enablement Program

GitLab has a growing [resource](/handbook/people-group/learning-and-development/manager-development/) to enable all team members transitioning to a manager role. It contains a link to a checklist, readings, and a form to help learning and development customize your development as a manager.

## Learning Management Systems

We are currently evaluating different learning management systems (LMS) to provide growth opportunities in a more structured, on-demand format. To help us continue to be [Handbook First](/handbook/handbook-usage/#why-handbook-first), course content should be structured in the following way:

- Text in handbook
- Video on Youtube
- Test and certification in an open source platform or something without a price per user/student so we can invite all our million of users without incremental costs.
- Don't put text or video in the platform itself, only link to the handbook and video, so we  have a [single source of truth](/handbook/handbook-usage/#style-guide-and-information-architecture) and don't end up with duplicate content that is hard to keep up to date.

## Common Ground: Harassment Prevention Training

All team members will be sent an invitation link from the people team to complete this training using [Will Interactive's LMS](https://learning.willinteractive.com/). Once you receive the email please do the following:

1. Underneath the green **Sign In** box, click on the **Sign Up Now** link (also in green) which is right after *Don't have an account?*
1. Enter in your name and GitLab email address
1. Create a password
1. You may be sent a link to verify your account
1. Once you have logged in successfully you will be taken to your home screen
1. Once there you should see the course title **GitLab for Supervisors: Common Ground Business for Supervisors** or **GitLab for Employees:Common Ground Business for Employees**, if you live in California the course title will include CA within it.
1. Click on that to begin the training.
1. For managers and leaders, this is 2 hours long, but you can stop and come back to it. For all other GitLab team-members, this is 1 hour long.
1. You can also use the navigation bar at the top right-hand side of screen for volume and screen settings
1. To the left and right of the center screen you should see this symbol: > which you can click on to move forward and back through the training
1. Once completed, please upload a copy of your certificate in BambooHR in the *verification* folder
1. You may also keep a record of the certificate for your own files. To create the certificate, click on *view* in the course title
1. Scroll down to *users* then click on *completion certificates* to download the PDFs

Our [Anti-Harassment Policy](/handbook/anti-harassment/?private=1) outlines guidelines, reporting and disciplinary action.  

If you have any questions or need further help please ping people ops in the `#peopleops` channels in slack.

## Performance Indicators

### Engagement Survey Growth and Development Score > X%

Questions related to growth and development on the semi-annual [Engagement Survey](/handbook/people-operations/engagement/) have a favorable score. The exact target is to be determined.

### Rate of internal job promotions > X%

Total number of [promotions](/handbook/people-operations/promotions-transfers/) in a rolling six month period/total number of employees. The target for this is to be determined.

### 12 month voluntary team member turnover related to growth < X%
This is calculated the same as [12 month voluntary team member turnover KPI](/handbook/people-operations/people-operations-metrics/#team-member-turnover) but is using the number of team members actively choosing to leave GitLab to growth and development related reasons only. The target is to be determined.
