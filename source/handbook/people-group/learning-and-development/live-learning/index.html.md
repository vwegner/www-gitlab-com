---
layout: handbook-page-toc
title: Live Learning
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Live Learning Schedule

1. The 2019 Live Learning schedule is as follows: 
   - December - Tentative: Coaching
1. The 2020 Live Learning schedule is as follows: 
   - January - TBC
   - February - TBC
   - March - TBC
   - April - TBC
   - May - TBC
   - June - TBC
   - July - TBC
   - August - TBC
   - September - TBC
   - October - TBC
   - November - TBC
   - December - TBC

## Past Live Learning Sessions
### 2019:
1. October - **Communicating Effectively Through Text**
   - [Slide deck](https://docs.google.com/presentation/d/1u_eob52HOrIwP3LGB4zMSOSEpMWFeCKxxFKxiQJ6YG8/edit?usp=sharing) 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://youtu.be/trYrbPYOf_c" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
